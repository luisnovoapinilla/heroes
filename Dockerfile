FROM adoptopenjdk/openjdk11:alpine-jre
COPY target/heroes*.jar /heroes.jar
EXPOSE 8080
CMD ["/heroes.jar"]