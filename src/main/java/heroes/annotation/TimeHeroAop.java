package heroes.annotation;

import heroes.exception.HeroNotFoundMethodException;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * Implemented logic class for annotation TimeHero
 */
@Aspect
@Component
@EnableAspectJAutoProxy
@Slf4j
public class TimeHeroAop {

    /**
     * logic implemented for time of request
     * @param point
     * @param timeHero
     * @return
     * @throws Throwable
     */
    @Around("@annotation(timeHero)")
    public Object around(ProceedingJoinPoint point, TimeHero timeHero) throws Throwable {
        //get actual method
        var method = getCurrentMethod(point);
        String methodName = method.getName();
        log.info("Method with name: " + methodName);

        try {
            //get time start
            final long startMills = System.currentTimeMillis();
            log.info("Start Mills: " + startMills);

            //proceed
            Object result = point.proceed();

            //get value from annotation
            final long slowThreshold = timeHero.timeHeroMillis();

            if (slowThreshold >= 0) {
                final long endMills = System.currentTimeMillis();
                //calculate cost time
                long costTime = endMills - startMills;

                log.info(methodName + " cost time is " + costTime + " ms.");
            }

            return result;
        } catch (Throwable e) {
            log.error(methodName + " Error ex." + e.getMessage());
            throw e;
        }
    }

    private Method getCurrentMethod(ProceedingJoinPoint point) {
        try {
            var sig = point.getSignature();
            MethodSignature msig = (MethodSignature) sig;
            Object target = point.getTarget();
            return target.getClass().getMethod(msig.getName(), msig.getParameterTypes());
        } catch (NoSuchMethodException e) {
            throw new HeroNotFoundMethodException(e);
        }
    }
}


