package heroes.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;

/**
 * Config class SecurityConfig
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /** User name */
    @Value("${heroes.user.name}")
    private String nameUser;

    /** User name */
    @Value("${heroes.user.password}")
    private String passwordUser;

    /** User name */
    @Value("${heroes.admin.name}")
    private String nameAdmin;

    /** User name */
    @Value("${heroes.admin.password}")
    private String passwordAdmin;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/**").permitAll()
                .anyRequest().authenticated().and()
                .headers().frameOptions().disable()
                .and()
                .csrf().ignoringAntMatchers("/**")
                .and()
                .csrf().disable()
                .httpBasic();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        var encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        auth.inMemoryAuthentication()
                .withUser(nameAdmin).password(encoder.encode(passwordAdmin))
                .roles("ADMIN").and()
                .withUser(nameUser).password(encoder.encode(passwordUser))
                .roles("USER");
    }
}