package heroes.controller;

import heroes.annotation.TimeHero;
import heroes.exception.ControllerAdvisor;
import heroes.exception.HeroNoDataFoundException;
import heroes.exception.HeroNotFoundException;
import heroes.service.HeroManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Api rest HeroesController
 */
@RestController
@Api (value = "Api Hero | Controller Maintenance Hero")
@Slf4j
public class HeroesController {

    private static final String MSG_DELETE = "Deleted Hero!!";

    @Autowired
    private HeroManager service;

    @Autowired
    private ControllerAdvisor controllerAdvisor;

    /**
     * allHeroes
     * @return heroes list
     */
    @TimeHero
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    @ApiOperation(value = "Method to list all Heroes.",
            nickname = "all",
            response = ResponseEntity.class, tags = { "heroes-controller", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Correct response", response = ResponseEntity.class),
            @ApiResponse(code = 401, message = "Unauthorized. Are the headers correct?"),
            @ApiResponse(code = 403, message = "Forbidden. You don´t have permission to accesss to this server"),
            @ApiResponse(code = 404, message = "Not Found. The element is not found!") })
    @GetMapping(value= "/api/", produces = { "application/json" })
    public ResponseEntity<Object> all() {
        log.info("Entry method api list heroes");
        try {
            return ResponseEntity.ok(service.all());
        } catch (HeroNoDataFoundException e) {
            return controllerAdvisor.handleNodataFoundException(e);
        }
    }

    /**
     * getHero
     * @param heroId param id hero
     * @return hero by id
     */
    @TimeHero
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    @ApiOperation(value = "Method to get one hero by id.",
            nickname = "get",
            response = ResponseEntity.class, tags = { "heroes-controller", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Correct response", response = ResponseEntity.class),
            @ApiResponse(code = 401, message = "Unauthorized. Are the headers correct?"),
            @ApiResponse(code = 403, message = "Forbidden. You don´t have permission to accesss to this server"),
            @ApiResponse(code = 404, message = "Not Found. The element is not found!") })
    @GetMapping(value= "/api/{id}", produces = { "application/json" })
    public ResponseEntity<Object> get(@ApiParam(value = "ID of hero", required = true, example = "1", defaultValue = "0")
                                          @PathVariable("id") Long heroId) {
        log.info("Entry method api get hero");
        try {
            return ResponseEntity.ok(service.get(heroId));
        } catch (HeroNotFoundException e) {
            return controllerAdvisor.handleHeroNotFoundException(e);
        }
    }

    /**
     * searchHero
     * @param text param name hero
     * @return list with results
     */
    @TimeHero
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    @ApiOperation(value = "Method to search all heroes by contains text.",
            nickname = "find",
            response = ResponseEntity.class, tags = { "heroes-controller", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Correct response", response = ResponseEntity.class),
            @ApiResponse(code = 401, message = "Unauthorized. Are the headers correct?"),
            @ApiResponse(code = 403, message = "Forbidden. You don´t have permission to accesss to this server"),
            @ApiResponse(code = 404, message = "Not Found. The element is not found!") })
    @GetMapping(value= "/api/find/{text}", produces = { "application/json" })
    public ResponseEntity<Object> find(@ApiParam(value = "Name of hero", required = true, example = "C.Man")
                                           @PathVariable("text") String text) {
        log.info("Entry method api find heroes");
        try {
            return ResponseEntity.ok(service.search(text));
        } catch (HeroNoDataFoundException e) {
            return controllerAdvisor.handleNodataFoundException(e);
        }
    }

    /**
     * modifyHero
     * @param heroId param id hero
     * @param heroName param name hero
     * @return update hero
     */
    @TimeHero
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Method to modify hero by id with new name.",
            nickname = "modify",
            response = ResponseEntity.class, tags = { "heroes-controller", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Correct response", response = ResponseEntity.class),
            @ApiResponse(code = 401, message = "Unauthorized. Are the headers correct?"),
            @ApiResponse(code = 403, message = "Forbidden. You don´t have permission to accesss to this server"),
            @ApiResponse(code = 404, message = "Not Found. The element is not found!") })
    @PutMapping(value= "/api/{id}/{name}", produces = { "application/json" })
    public ResponseEntity<Object> modify(@ApiParam(value = "ID of hero", required = true, example = "1")
                                             @PathVariable(value = "id") Long heroId,
                                         @ApiParam(value = "Name of hero", required = true, example = "C. Man")
                                         @PathVariable(value = "name") String heroName) {
        log.info("Entry method api modify hero");
        try {
            return ResponseEntity.ok(service.modify(heroId, heroName));
        } catch (HeroNotFoundException e) {
            return controllerAdvisor.handleHeroNotFoundException(e);
        }
    }

    /**
     * deleteHero
     * @param heroId param id hero
     * @return Delete hero by id
     */
    @TimeHero
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Method to delete hero by id.",
            nickname = "delete",
            response = ResponseEntity.class, tags = { "heroes-controller", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Correct response", response = ResponseEntity.class),
            @ApiResponse(code = 401, message = "Unauthorized. Are the headers correct?"),
            @ApiResponse(code = 403, message = "Forbidden. You don´t have permission to accesss to this server"),
            @ApiResponse(code = 404, message = "Not Found. The element is not found!") })
    @DeleteMapping(value= "/api/{id}", produces = { "application/json" })
    public ResponseEntity<Object> delete(@ApiParam(value = "ID of hero", required = true, example = "1")
                                             @PathVariable(value = "id") Long heroId) {
        log.info("Entry method api delete hero");
        try {
            service.delete(heroId);
            return ResponseEntity.ok(MSG_DELETE);
        } catch (HeroNotFoundException e) {
            return controllerAdvisor.handleHeroNotFoundException(e);
        }
    }
}
