package heroes.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Class Manager Exception
 */
@ControllerAdvice
public class ControllerAdvisor {

    /**
     * handleHeroNotFoundException
     * @param ex param Exception
     * @return response cause of error
     */
    @ExceptionHandler(HeroNotFoundException.class)
    public ResponseEntity<Object> handleHeroNotFoundException(
            HeroNotFoundException ex) {

        return new ResponseEntity<>(mapLog("Hero not found"), HttpStatus.NOT_FOUND);
    }

    /**
     * handleNodataFoundException
     * @param ex param Exception
     * @return response cause of error
     */
    @ExceptionHandler(HeroNoDataFoundException.class)
    public ResponseEntity<Object> handleNodataFoundException(
            HeroNoDataFoundException ex) {

        return new ResponseEntity<>(mapLog("heroes not found"), HttpStatus.NOT_FOUND);
    }

    /**
     * handleMethodArgumentNotValid
     * @param ex param Exception
     * @param headers param headers request
     * @param status param status
     * @return response cause of error
     */
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers,
            HttpStatus status) {

        Map<String, Object> body = mapLog(String.valueOf(status.value()));

        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(x -> x.getDefaultMessage())
                .collect(Collectors.toList());

        body.put("errors", errors);

        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }

    /**
     * mapLog
     * @param message param message exception
     * @return map body message of error
     */
    private Map<String, Object> mapLog (String message){
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", message);

        return body;
    }
}