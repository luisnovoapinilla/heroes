package heroes.exception;

/**
 * HeroNoDataFoundException
 */
public class HeroNoDataFoundException extends RuntimeException {

    /**
     * NoDataFoundException
     */
    public HeroNoDataFoundException() {

        super("No data found");
    }
}