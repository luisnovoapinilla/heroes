package heroes.exception;

/**
 * HeroNotFoundException
 */
public class HeroNotFoundException extends RuntimeException {

    /**
     * HeroNotFoundException
     * @param id
     */
    public HeroNotFoundException(Long id) {

        super(String.format("Hero with Id %d not found", id));
    }
}