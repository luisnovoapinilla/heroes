package heroes.exception;

/**
 * HeroNotFoundMethodException
 */
public class HeroNotFoundMethodException extends RuntimeException {

    /**
     * HeroNotFoundMethodException
     * @param e
     */
    public HeroNotFoundMethodException(Exception e) {

        super(String.format("Not found method by %d"), e.getCause());
    }
}