package heroes.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Class JPA entity Hero
 */
@Getter
@Setter
@Entity
@AllArgsConstructor
@Table(name = "heroes")
@ApiModel(value = "Hero", description = "Table with different name of heroes")
public class Hero {

    @ApiModelProperty( value  =  "Unique Id for each hero")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ApiModelProperty( value  =  "Name for each hero")
    @Column(name = "NAME", length = 50, nullable = false, unique = true)
    private String name;

    public Hero() {
    }

    public Hero(String name) {
        this.name = name;
    }

}
