package heroes.repository;

import heroes.model.Hero;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * HeroesRepository
 */
@Repository
public interface HeroesRepository extends JpaRepository<Hero, Long> {

    /**
     * findByNameContaining
     * @param name
     * @return list heroes if contains param on filed name
     */
    @Query("SELECT h FROM Hero h WHERE h.name LIKE %:name%")
    List<Hero> findByNameContaining(String name);

}
