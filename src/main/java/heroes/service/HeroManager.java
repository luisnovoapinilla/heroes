package heroes.service;

import heroes.model.Hero;

import java.util.List;

/**
 * HeroManager
 */
public interface HeroManager {

    List<Hero> all();

    Hero get(Long id);

    List<Hero> search(String text);

    Hero modify(Long id, String name);

    void delete(Long id);


}