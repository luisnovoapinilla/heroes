package heroes.service.impl;


import heroes.exception.HeroNoDataFoundException;
import heroes.exception.HeroNotFoundException;
import heroes.model.Hero;
import heroes.repository.HeroesRepository;
import heroes.service.HeroManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service logic HeroManagerImpl
 */
@Service
@Slf4j
public class HeroManagerImpl implements HeroManager {

    @Autowired
    private HeroesRepository heroesRepository;

    /**
     * All heroes
     * @return List<Hero>
     */
    @Override
    public List<Hero> all() {
        List<Hero> list = heroesRepository.findAll();
        if (list.isEmpty()) {
            throw new HeroNoDataFoundException();
        }
        return list;
    }

    /**
     * Hero by id
     * @param id param id hero
     * @return Hero
     */
    @Cacheable("getHero")
    @Override
    public Hero get(Long id) {
        return heroesRepository.findById(id).orElseThrow(() -> new HeroNotFoundException(id));
    }

    /**
     * search by contain text
     * @param text para name hero
     * @return List<Hero>
     */
    @Override
    public List<Hero> search(String text) {
        List<Hero> list = heroesRepository.findByNameContaining(text);
        if (list.isEmpty()) {
            throw new HeroNoDataFoundException();
        }
        return list;
    }

    /**
     * Update hero by id
     * @param id param id hero to modify
     * @param name param name hero to modify
     * @return Hero
     */
    @Override
    @CachePut(cacheNames = "getHero", key = "#id")
    public Hero modify(Long id, String name) {
        Hero hero = heroesRepository.findById(id).orElseThrow(() -> new HeroNotFoundException(id));
        hero.setName(name);
        return heroesRepository.save(hero);
    }

    /**
     * Delete hero by id
     * @param id para id hero to delete
     */
    @Override
    @CacheEvict(cacheNames = "getHero", key = "#id")
    public void delete(Long id) {
        heroesRepository.findById(id).orElseThrow(() -> new HeroNotFoundException(id));
        heroesRepository.deleteById(id);
    }
}