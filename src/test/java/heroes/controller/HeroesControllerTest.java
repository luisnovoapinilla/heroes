package heroes.controller;

import heroes.HeroApplication;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = HeroApplication.class, webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
class HeroesControllerTest {

    private static final String USER = "user";
    private static final String PASSUSER = "joker";
    private static final String ADMIN = "admin";
    private static final String PASSADMIN = "rekoj";
    private static final String url = "http://localhost:8080";

    private TestRestTemplate restTemplate;

    @Test
    void testAllHeroes() {

        restTemplate = new TestRestTemplate(USER, PASSUSER);
        ResponseEntity<Object> response = restTemplate.getForEntity(url.concat("/heroes/api/"), Object.class);

        assertThat(response.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void testGetHero() {

        restTemplate = new TestRestTemplate(USER, PASSUSER);
        ResponseEntity<Object> response = restTemplate.getForEntity(url.concat("/heroes/api/1"), Object.class);

        assertThat(response.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void testSearchHero() {

        restTemplate = new TestRestTemplate(USER, PASSUSER);
        ResponseEntity<Object> response = restTemplate.getForEntity(url.concat("/heroes/api/find/Th"), Object.class);

        assertThat(response.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void testSearchHero_HeroManagerReturnsNoItems() throws Exception {

        restTemplate = new TestRestTemplate(USER, PASSUSER);
        ResponseEntity<Object> response = restTemplate.getForEntity(url.concat("/heroes/api/find/Ths"), Object.class);

        assertThat(response.getStatusCodeValue()).isEqualTo(HttpStatus.NOT_FOUND.value());

    }

    @Test
    void testModifyHero() {
        restTemplate = new TestRestTemplate(ADMIN, PASSADMIN);

        Map<String, Object> vars = new HashMap<>();
        vars.put("id", 1L);
        vars.put("name", "Thora");

        restTemplate.put(url.concat("/heroes/api/{id}/{name}"), null, vars);

    }

    @Test
    void testDeleteHero() {
        restTemplate = new TestRestTemplate(ADMIN, PASSADMIN);
        Map<String, Object> vars = new HashMap<>();
        vars.put("id", 1L);

        restTemplate.delete(url.concat("/heroes/api/{id}"), null, vars);
    }
}
