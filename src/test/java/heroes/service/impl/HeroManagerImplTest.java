package heroes.service.impl;

import heroes.model.Hero;
import heroes.repository.HeroesRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class HeroManagerImplTest {

    @Mock
    private HeroesRepository mockHeroesRepository;

    @InjectMocks
    private HeroManagerImpl heroManagerImplUnderTest;

    @Test
    void testAll() {
        final List<Hero> expectedResult = List.of(new Hero(1L, "Hero1"));
        when(mockHeroesRepository.findAll()).thenReturn(List.of(new Hero(1L, "Hero1")));

        final List<Hero> result = heroManagerImplUnderTest.all();

        assertThat(result.get(0).getId()).isEqualTo(expectedResult.get(0).getId());
    }

    @Test
    void testGet() {
        final Hero expectedResult = new Hero(1L, "Hero1");
        when(mockHeroesRepository.findById(0L)).thenReturn(Optional.of(new Hero(1L,"Hero1")));

        final Optional<Hero> result = Optional.ofNullable(heroManagerImplUnderTest.get(0L));

        assertThat(result.get().getId()).isEqualTo(expectedResult.getId());
    }

    @Test
    void testDelete() {
        when(mockHeroesRepository.findById(0L)).thenReturn(Optional.of(new Hero("Hero1")));
        heroManagerImplUnderTest.delete(0L);
        verify(mockHeroesRepository).deleteById(0L);
    }
}
